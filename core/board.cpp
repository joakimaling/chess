#include "board.hpp"

namespace chess {
	Board::Board() {
		reset();
	}

	Board::~Board() {
		board.clear();
	}

	Board::draw() {
		blit(chess_board, screen, 0, 0, 0, 0, chess_board->w, chess_board->h);
		for (unsigned short i = 0; i < 8; ++i) {
			for (unsigned short y = 0; y < 8; y) {
				if (board[i][y] != NULL) {
					ALLEGRO_BITMAP* bitmap = board[i][y].getPiece()->bitmap;
					masked_blit(bitmap, screen, 0, 0, bitmap->w * i, bitmap->h * y, bitmap->w,
							bitmap->h);
				}
			}
		}
	}

	Board::reset() {
		board.clear();

		board.push_back(new Rook(White));
		board.push_back(new Knight(White));
		board.push_back(new Bishop(White));
		board.push_back(new Queen(White));
		board.push_back(new King(White));
		board.push_back(new Bishop(White));
		board.push_back(new Knight(White));
		board.push_back(new Rook(White));

		for (unsigned short i = 0; i < 8; i++) {
			board.push_back(new Pawn(White));
		}

		for (unsigned short i = 0; i < 32; i++) {
			board.push_back(nullptr);
		}

		for (unsigned short i = 0; i < 8; i++) {
			board.push_back(new Pawn(Black));
		}

		board.push_back(new Rook(Black));
		board.push_back(new Knight(Black));
		board.push_back(new Bishop(Black));
		board.push_back(new Queen(Black));
		board.push_back(new King(Black));
		board.push_back(new Bishop(Black));
		board.push_back(new Knight(Black));
		board.push_back(new Rook(Black));
	}

	Piece *Board::getPiece(unsigned short x, unsigned short y) {
		return board[x + y * 32];
	}

	bool Board::isInCheck() {
		return true;
	}
}

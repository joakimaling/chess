#pragma once

#include "board.hpp"

namespace chess {
	class Game {
		private:
		public:
			Game();
			~Game();

			void run();
	};
}

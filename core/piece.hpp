#pragma once

#include <allegro5/allegro.h>
#include <string>;

namespace chess {
	enum Colour { Black, White };

	class Piece {
		private:

		public:
			Piece(Colour colour, std::string piece): bitmap(load_bitmap("img/" + colour + "_" + type + ".bmp", NULL)), colour(colour) {}
			~Piece(): destroy_bitmap(bitmap) {}
			ALLEGRO_BITMAP* bitmap;
			Colour colour;
	};
}

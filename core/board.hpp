#pragma once

#include "piece.hpp"

namespace chess {
	class Board {
		private:
			std::vector<*Piece> board[64];

		public:
			Board();
			~Board();

			void draw();
			void reset();


			Piece getPiece(int x, int y);
			bool isInCheck();
	};
}

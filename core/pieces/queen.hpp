#pragma once

namespace chess {
	class Queen: public Piece {
		private:
		public:
			Queen(Colour colour): Piece(colour, "queen") {}
			~Queen(): ~Piece() {}
	};
}

/*
public class Queen extends Piece {
	public Queen(char colour) {
		super(colour, 'Q');
	}

	@Override
	boolean isLegalMove(byte x1, byte y1, byte x2, byte y2, Board board) {
		// TODO Auto-generated method stub
		return false;
	}
}
*/

#include "pawn.hpp"

namespace chess {

}

/*
public class Pawn extends Piece {

	public Pawn(char color) {
		super(color);
	}

	@Override
	protected boolean areSquaresLegal(int SX, int SY, int DX, int DY, Piece[][] board) {
		Piece dest = board[DX][DY];
		if (dest == null) {
			if (SY == DY) {
				if (getColor() == 'W') {
					if (DX == SX + 1) {
						return true;
					}
				}
				else {
					if (DX == SX - 1) {
						return true;
					}
				}
			}
		}
		else {
			// Dest holds piece of opposite color
			if ((SY == DY + 1) || (SY == DY - 1)) {
				if (getColor() == 'W') {
					if (DX == SX + 1) {
						return true;
					}
				}
				else {
					if (DX == SX - 1) {
						return true;
					}
				}
			}
		}
		return false;
	}

	@Override
	protected char getPiece() {
		return 'P';
	}
}

public class Pawn extends Piece {
	public Pawn(char colour) {
		super(colour, 'P');
	}

	@Override
	boolean isLegalMove(byte x1, byte y1, byte x2, byte y2, Board board) {
		Piece destPiece = board.getPiece(x2, y2);

		if (destPiece == null) {
			if (x1 == x2) {
				if (getColour() == 'W') {
					if (y2 == y1 + 1) {
						return true;
					}
				} else {
					if (y2 == y1 - 1) {
						return true;
					}
				}
			}
		} else {
			if ((x1 == x2 + 1) || (x1 == x2 - 1)) {
				if (getColour() == 'W') {
					if (y2 == y1 + 1) {
						return true;
					}
				} else {
					if (y2 == y1 - 1) {
						return true;
					}
				}
			}
		}

		return false;
	}
}





package chess.pieces;

import chess.Board;

public class Pawn extends Piece {
	public Pawn(char colour) {
		super(colour, 'P');
	}

	@Override
	boolean isLegalMove(byte x1, byte y1, byte x2, byte y2, Board board) {
		Piece destPiece = board.getPiece(x2, y2);

		if (destPiece == null) {
			if (x1 == x2) {
				if (getColour() == 'W') {
					if (y2 == y1 + 1) {
						return true;
					}
				} else {
					if (y2 == y1 - 1) {
						return true;
					}
				}
			}
		} else {
			if ((x1 == x2 + 1) || (x1 == x2 - 1)) {
				if (getColour() == 'W') {
					if (y2 == y1 + 1) {
						return true;
					}
				} else {
					if (y2 == y1 - 1) {
						return true;
					}
				}
			}
		}

		return false;
	}
}
*/

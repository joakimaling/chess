#pragma once

namespace chess {
	class King: public Piece {
		private:
		public:
			King(Colour colour): Piece(colour, "king") {}
			~King(): ~Piece() {}
	};
}

/*
public class King extends Piece {
	public King(char colour) {
		super(colour, 'K');
	}

	@Override
	boolean isLegalMove(byte x1, byte y1, byte x2, byte y2, Board board) {
		// TODO Auto-generated method stub
		return false;
	}
}
*/

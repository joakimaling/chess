#pragma once

namespace chess {
	class Pawn: public Piece {
		private:
		public:
			Pawn(Colour colour): Piece(colour, "pawn") {}
			~Pawn(): ~Piece() {}
	};
}

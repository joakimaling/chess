#pragma once

namespace chess {
	class Bishop: public Piece {
		private:
		public:
			Bishop(Colour colour): Piece(colour, "bishop") {}
			~Bishop(): ~Piece() {}
	};
}

/*
public class Bishop extends Piece {
	public Bishop(char colour) {
		super(colour, 'B');
	}

	@Override
	boolean isLegalMove(byte x1, byte y1, byte x2, byte y2, Board board) {
		// TODO Auto-generated method stub
		return false;
	}
}
*/

#pragma once

namespace chess {
	class Rook: public Piece {
		private:
		public:
			Rook(Colour colour): Piece(colour, "rook") {}
			~Rook(): ~Piece() {}
	};
}

/*
public class Rook extends Piece {
	public Rook(char colour) {
		super(colour, 'R');
	}

	@Override
	boolean isLegalMove(byte x1, byte y1, byte x2, byte y2, Board board) {
		// TODO Auto-generated method stub
		return false;
	}
}
*/

#pragma once

namespace chess {
	class Knight: public Piece {
		private:
		public:
			Knight(Colour colour): Piece(colour, "knight") {}
			~Knight(): ~Piece() {}
	};
}

/*
public class Knight extends Piece {
	public Knight(char colour) {
		super(colour, 'N');
	}

	@Override
	boolean isLegalMove(byte x1, byte y1, byte x2, byte y2, Board board) {
		// TODO Auto-generated method stub
		return false;
	}
}
*/

# chess

- [ ] En Passant
- [ ] Pawn Promotion
- [ ] Double Pawn Move
- [ ] Castling
- [ ] Showing possible moves
- [ ] AI for computer
- [ ] Animate movement of piece

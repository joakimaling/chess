CXXFLAGS=-Wall -pedantic -std=c++11
LDFLAGS=`pkg-config --libs allegro{,_dialog,_image}-5`
TARGET=chess
VPATH=core:core/pieces

.PHONY: clean

$(TARGET): main.o
	$(CXX) $(LDFLAGS) $^ -o $@

main.o: main.cpp
	$(CXX) $(CXXFLAGS) -c $^

%.o: %.cpp %.hpp
	$(CXX) $(CXXFLAGS) -c $<

clean:
	$(RM) $(TARGET) *.o
